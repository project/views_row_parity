Views Row Parity

This module creates a views handler that will let the users use a global field for checking the row parity.

The only possible values that this handler will ever return are "odd" and "even".

This can be used in combination with views_conditionnal to provide extra an alternate layout based on the row parity.
